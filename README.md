
## Requirements

---

Install dependencies

```pip install -r requirements.txt```


Install poppler and add the path of Poppler_dir/bin to environment.

[Windows](http://blog.alivate.com.au/poppler-windows/)


## Running the application

---

```flask run```
